/* eslint-disable */
export default function ({store, $axios, $auth, redirect }, inject) {
  // Create a custom api instance
  const api = $axios.create({
    timeout: 10000,
  })

  api.onRequest((config) => {
    console.log('request')
    store.commit('loader/show')
  })

  api.onResponse((config) => {
    store.commit('loader/hide')
  })

  api.onError((error) => {
    store.commit('loader/hide')
  })

  inject('axios', api)
}
