export const state = () => ({
  requestsPending: 0,
});

export const getters = {
  isLoading(state) {
    return state.requestsPending > 0;
  },
};

export const mutations = {
  show(state) {
    state.requestsPending++;
  },
  hide(state) {
    state.requestsPending--;
  },
};
