export const state = () => ({
  questions: [],
  currentQuestionIndex: 0,
  resultMode: false,
});
export const mutations = {
  setQuestions(state, questions) {
    questions.map((q) => {
      q.answers = [...q.incorrect_answers, q.correct_answer];
      q.selectedAnswer = null;
    });
    state.questions = questions;
  },
  setCurrentQuestion(state, questionIndex) {
    state.currentQuestionIndex = questionIndex;
  },
  setAnswer(state, answer) {
    state.questions[state.currentQuestionIndex].selectedAnswer = answer;
  },
  enableResultMode(state) {
    state.resultMode = true;
  },
  disableResultMode(state) {
    state.resultMode = false;
    state.questions = [];
  },
};

export const actions = {
  goToQuestion({ state, commit }, questionIndex) {
    if (questionIndex < state.questions.length && questionIndex >= 0) {
      commit("setCurrentQuestion", questionIndex);
    }
  },
  clearAnswer({ commit }) {
    commit("setAnswer", null);
  },
  async fetchQuestions({ commit }, params) {
    let url = `https://opentdb.com/api.php?amount=${params.amount || 5}`;
    if (params.category) {
      url += `&category=${params.category}`;
    }
    if (params.difficulty) {
      url += `&difficulty=${params.difficulty.toLowerCase()}`;
    }
    const res = await this.$axios.get(url);
    commit("setQuestions", res.data.results);
  },
  end() {
    this.$router.push("/result");
  },
};

export const getters = {
  currentQuestion(state) {
    return state.questions[state.currentQuestionIndex];
  },
  answeredCount(state) {
    return state.questions.filter((q) => q.selectedAnswer != null).length;
  },
  completeRate(state, getters) {
    return (getters.answeredCount / state.questions.length) * 100;
  },
  corrects(state) {
    return state.questions.filter((q) => q.selectedAnswer === q.correct_answer);
  },
  wrongs(state) {
    return state.questions.filter((q) => q.selectedAnswer && q.selectedAnswer !== q.correct_answer);
  },
  unanswered(state) {
    return state.questions.filter((q) => !q.selectedAnswer);
  },
};
